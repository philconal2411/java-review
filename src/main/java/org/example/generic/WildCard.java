package org.example.generic;

public class WildCard<T> {
    private T animal;

    public WildCard(T animal) {
        this.animal = animal;
    }

    public T getAnimal() {
        return animal;
    }
}
