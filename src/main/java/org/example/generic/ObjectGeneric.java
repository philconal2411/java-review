package org.example.generic;

public class ObjectGeneric <T>{
    private T object;
    public ObjectGeneric(Class<T> tClass) throws  InstantiationException,IllegalAccessException{

        this.object = (T) tClass.newInstance();
    }

    public T getObject() {
        return object;
    }
}
