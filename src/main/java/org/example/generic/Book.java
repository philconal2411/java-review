package org.example.generic;

public class Book<K,V, T>  extends  Dictionary<K,V> implements GenericInterface<T,K,V>{
    public Book(K englishWord, V vietnameseWord) {
        super(englishWord, vietnameseWord);
    }

public Book(){

        super();
}
    @Override
    public void update(T obj) {

    }

    @Override
    public void change(T obj1, K obj2) {

    }

    @Override
    public void delete(V obj) {

    }
}
