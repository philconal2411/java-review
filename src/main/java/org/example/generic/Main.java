package org.example.generic;

import org.example.inheritance.animal.Animal;

public class Main {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
//        Dictionary<String, String> dictionary = new Dictionary<>("learn", "Hoc");
//        System.out.println(dictionary.getVietnameseWord());
//        Dictionary<Character, String> dictionary1 = new Dictionary<>('l', "Hoc");
//        System.out.println(dictionary1.getVietnameseWord());
//
//        Book<Integer, String,String> book = new Book<>(1, "Soi");
//        System.out.println(book.getVietnameseWord());
//        ObjectGeneric<Book> objectGeneric = new ObjectGeneric<>(Book.class);
//
//        System.out.println(objectGeneric.getObject().getVietnameseWord());
        WildCard<? extends Animal> object = new WildCard<>(new Animal("Conal"));
        System.out.println(object.getAnimal().getName() );
    }
}
