package org.example.generic;

public interface GenericInterface<T,K,V> {
    void update(T obj);
    void change(T obj1, K obj2);
    void delete(V obj);
}
