package org.example.generic;

public class Dictionary <K,V>{
    private K englishWord;
    private V vietnameseWord;

    public Dictionary(K englishWord, V vietnameseWord) {
        this.englishWord = englishWord;
        this.vietnameseWord = vietnameseWord;
    }

    public Dictionary() {
    }

    public K getEnglishWord() {
        return englishWord;
    }

    public void setEnglishWord(K englishWord) {
        this.englishWord = englishWord;
    }

    public V getVietnameseWord() {
        return vietnameseWord;
    }

    public void setVietnameseWord(V vietnameseWord) {
        this.vietnameseWord = vietnameseWord;
    }
}
