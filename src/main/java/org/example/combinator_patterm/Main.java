package org.example.combinator_patterm;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Customer customer = new Customer("Alice", "alcie@gmail.com", "0998987987", LocalDate.of(2000, 11, 24));
        CustomerValidatorService customerValidatorService = new CustomerValidatorService();
        boolean customerValidatorServiceValid = customerValidatorService.isValid(customer);
        System.out.println(customerValidatorServiceValid);
    }
}
