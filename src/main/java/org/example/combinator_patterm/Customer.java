package org.example.combinator_patterm;

import java.time.LocalDate;

public class Customer {
    private final String name;
    private final String email;
    private final String phoneNumber;
    private final LocalDate doc;

    public Customer(String name, String email, String phoneNumber, LocalDate doc) {
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.doc = doc;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public LocalDate getDoc() {
        return doc;
    }
}
