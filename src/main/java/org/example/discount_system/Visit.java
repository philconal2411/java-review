package org.example.discount_system;

import java.util.Date;

public class Visit extends Customer {
    private Date date;
    private double serviceExpense;
    private double productExpense;

    public Visit(String name, boolean isMember, String memberType) {
        super(name, isMember, memberType);
    }

    public double getServiceExpense() {
        return serviceExpense;
    }
    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public double getTotalExpense() {
        double serviceDiscount = super.isMember() ? this.getServiceDiscountRate(super.getMemberType()) : 0;
        double productDiscount = super.getProductDiscountRate();
        return (this.serviceExpense - (this.serviceExpense * serviceDiscount)) + (this.productExpense - (this.productExpense * productDiscount));
    }

    @Override
    public String toString() {
        return "Visit{" +
                "date=" + date +
                ", serviceExpense=" + serviceExpense +
                ", productExpense=" + productExpense +
                "} " + super.toString();
    }
}
