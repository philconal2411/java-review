package org.example.discount_system;

public enum MemberType {
    PREMIUM("PREMIUM"), GOLD("GOLD"), SILVER("SILVER");
    private final String type;

    MemberType(String type) {
        this.type = type;
    }

    public String getMemberType() {
        return this.type;
    }

}
