package org.example.discount_system;

public abstract class Discount {
    private  final double PRODUCT_DISCOUNT =0.1;
    private final double serviceDiscountPremium = 0.2;
    private final double serviceDiscountGold = 0.15;
    private final double serviceDiscountSilver = 0.1;
//    private final double productDiscountPremium = 0.1;
//    private final double productDiscountGold = 0.1;
//    private final double productDiscountSilver = 0.1;

    public double getServiceDiscountRate(String type) {
        switch (type) {
            case "PREMIUM":
                return this.serviceDiscountPremium;
            case "GOLD":
                return this.serviceDiscountGold;
            case "SILVER":
                return this.serviceDiscountSilver;
            default:
                return 0;
        }
    }

    public double getProductDiscountRate() {
       return PRODUCT_DISCOUNT;
    }
}
