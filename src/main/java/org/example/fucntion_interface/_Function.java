package org.example.fucntion_interface;

import java.util.function.BiFunction;
import java.util.function.Function;

public class _Function {
    /* Function*/
    static Function<Integer, Integer> integerFunction = number -> number++;
    static Function<Integer, Integer> multipleBy10 = number -> number * 10;
    static Function<Integer, Integer> addBy1AndThenMultiplyBy10 = integerFunction.andThen(multipleBy10);

    /*---------------------*/
    static BiFunction<Integer, Integer, Integer> increaseByOneAndMultiplyFunction = (numberToIncreaseByOne, numberToMultiplyByOne) -> (numberToIncreaseByOne + 1) * numberToMultiplyByOne;

    public static void main(String[] args) {
        /* Function*/
        int increment = increment(0);
        System.out.println(increment);

        int apply = integerFunction.apply(increment);
        System.out.println(apply);

        int multiply = multipleBy10.apply(apply);
        System.out.println(multiply);

        int addBy1AndThenMultiplyBy10Result = addBy1AndThenMultiplyBy10.apply(4);
        System.out.println(addBy1AndThenMultiplyBy10Result);
        /*-------------------------*/
        /*Bi Function*/
        int increaseByOneAndMultiply = increaseByOneAndMultiplyFunction.apply(4,10);
        System.out.println(increaseByOneAndMultiply);

    }

    static int increment(int number) {
        return number + 1;
    }
}
