package org.example.fucntion_interface;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class _Consumer {
    static Consumer<Customer> greetingCustomerConsumer = customer -> System.out.println("Hello " + customer.customerName + ", thanks for registering phone number " + customer.customerPhoneNumber);
    static BiConsumer<Customer, Boolean> greetingCustomerBiConsumer = (customer, showPhoneNumber) -> System.out.println("Hello " + customer.customerName + (showPhoneNumber ? (", thanks for registering phone number " + customer.customerPhoneNumber) : ""));

    public static void main(String[] args) {
        /*Consumer*/
        Customer conal = new Customer("Conal", "99999");
        greetCustomer(conal);
        greetingCustomerConsumer.accept(conal);

        /*-----------------*/
        /*BiConsumer*/
        Customer thanh = new Customer("Thanh", "444444");
        greetingCustomerBiConsumer.accept(thanh,false);
    }

    static void greetCustomer(Customer customer) {
        System.out.println("Hello " + customer.customerName + ", thanks for registering phone number " + customer.customerPhoneNumber);
    }

    static class Customer {

        private final String customerName;
        private final String customerPhoneNumber;

        Customer(String customerName, String customerPhoneNumber) {
            this.customerName = customerName;
            this.customerPhoneNumber = customerPhoneNumber;
        }
    }
}
