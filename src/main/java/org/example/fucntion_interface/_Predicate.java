package org.example.fucntion_interface;

import java.util.function.Predicate;

public class _Predicate {
    static Predicate<String> isPhoneNumberValidPredicate = phoneNumber -> phoneNumber.startsWith("09") && phoneNumber.length() == 11;
    static Predicate<String> isContain3Predicate = phoneNumber -> phoneNumber.contains("3");

    public static void main(String[] args) {
        System.out.println(isPhoneNumberValid("0789080890809"));
        System.out.println(isPhoneNumberValid("09890808908"));
        /*Predicate*/
        System.out.println(isPhoneNumberValidPredicate.test("09909090909"));
        System.out.println(isPhoneNumberValidPredicate.or(isContain3Predicate).test("09890808908"));

    }

    static boolean isPhoneNumberValid(String phoneNumber) {
        return phoneNumber.startsWith("09") && phoneNumber.length() == 11;
    }
}
