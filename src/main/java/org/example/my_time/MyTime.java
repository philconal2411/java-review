package org.example.my_time;

interface MyTimeImpl {
    public MyTime nextSecond();

    public MyTime nextMinute();

    public MyTime nextHour();

    public MyTime previousSecond();

    public MyTime previousMinute();

    public MyTime previousHour();
}

public class MyTime implements MyTimeImpl {
    private int hour = 0;
    private int minute = 0;
    private int second = 0;

    public MyTime(int hour, int minute, int second) {
        this.setTime(hour, minute, second);
    }
    public void setTime(int hour, int minute, int second) {
//        try {
        this.setHour(hour);
        this.setMinute(minute);
        this.setSecond(second);
//        } catch (IllegalArgumentException e) {
//            System.out.println(e.getMessage());
//        }
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        if (hour < 0 || hour > 23)
            throw new IllegalArgumentException("Invalid hour");
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        if (minute < 0 || minute > 59)
            throw new IllegalArgumentException("Invalid minute");
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        if (second < 0 || second > 59)
            throw new IllegalArgumentException("Invalid second");
        this.second = second;
    }

    public MyTime nextSecond() {
        if (this.second == 59 && this.minute == 59 && this.hour == 23)
            this.setTime(0, 0, 0);
        else if (this.second == 59 && this.minute == 59)
            this.setTime(this.hour + 1, 0, 0);
        else if (this.second == 59)
            this.setTime(this.hour, this.minute + 1, 0);
        else
            this.setTime(this.hour, this.minute, this.second + 1);
        return this;
    }

    @Override
    public MyTime nextMinute() {
        if (this.minute == 59 && this.hour == 23)
            this.setTime(0, 0, 0);
        else if (this.minute == 59)
            this.setTime(this.hour + 1, 0, 0);
        else
            this.setTime(this.hour, this.minute + 1, 0);
        return this;
    }

    @Override
    public MyTime nextHour() {
        if (this.hour == 23)
            this.setTime(0, 0, 0);
        else
            this.setTime(this.hour + 1, 0, 0);
        return this;
    }

    @Override
    public MyTime previousSecond() {
        if (this.second == 0 && this.minute == 0 && this.hour == 0)
            this.setTime(23, 59, 59);
        else if (this.second == 0 && this.minute == 0)
            this.setTime(this.hour - 1, 59, 59);
        else if (this.second == 0)
            this.setTime(this.hour, this.minute - 1, 59);
        else
            this.setTime(this.hour, this.minute, this.second - 1);
        return this;
    }

    @Override
    public MyTime previousMinute() {
        if (this.minute == 0 && this.hour == 0)
            this.setTime(23, 59, 0);
        else if (this.minute == 0)
            this.setTime(this.hour - 1, 59, 0);
        else
            this.setTime(this.hour, this.minute - 1, 0);
        return this;
    }

    @Override
    public MyTime previousHour() {
        if (this.hour == 0)
            this.setTime(23, 0, 0);
        else
            this.setTime(this.hour - 1, 0, 0);
        return this;
    }

    @Override
    public String toString() {
        String hour = this.hour > 10 ? this.hour + "" : "0" + this.hour;
        String minute = this.minute > 10 ? this.minute + "" : "0" + this.minute;
        String second = this.second > 10 ? this.second + "" : "0" + this.second;
        return hour + ":" + minute + ":" + second;
    }
}
