package org.example.streams;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class _Stream {

    public static void main(String[] args) {

        List<Person> people = List.of(
                new Person("Conal", 21),
                new Person("Phil", 58),
                new Person("Data", 25),
                new Person("Computer", 41),
                new Person("Haunty", 11),
                new Person("CelPho", 41),
                new Person("Harry", 34));
        Predicate<Person> peopleOld = person -> person.age > 11;
        Function<Person, String> nameAndAge = person -> person.name + " " + person.age;
        List<String> collect = people.stream()
                .filter(peopleOld).distinct().map(nameAndAge).collect(Collectors.toList());
        collect.forEach(System.out::println);
        Predicate<Person> personPredicate = person -> person.age >= 19;
        boolean allMatch = people.stream().allMatch(personPredicate);
        System.out.println(allMatch);
    }

    static class Person {
        private final String name;
        private final int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }
    }
}
