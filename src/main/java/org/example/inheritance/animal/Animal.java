package org.example.inheritance.animal;

public class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public void setName() {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                '}';
    }
}
