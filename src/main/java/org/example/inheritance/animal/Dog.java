package org.example.inheritance.animal;

public class Dog extends Mammal {
    public Dog(String name) {
        super(name);
    }

    public void greets() {
        System.out.println("Woof");
    }

    @Override
    public String toString() {
        return "Dog{} " + super.toString();
    }

    public void greets(Dog anotherDog) {
        System.out.println(anotherDog.getName() + " Woof");
    }
}
