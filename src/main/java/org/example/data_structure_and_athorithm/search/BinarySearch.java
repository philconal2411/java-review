package org.example.data_structure_and_athorithm.search;

import java.util.Arrays;

public class BinarySearch {
    public static void main(String[] args) {
        int target = 42;
        int[] array = new int[100];
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }
        int index = binarySearch(array, target);
        System.out.println(index);
    }

    // 1. find middle position
    // 2. find a half of array matched with target
    // 3.
    private static String toString(int[] array) {
        String str = "";
        for (int item : array) {
            str += item + ", ";

        }
        return str;
    }

    private static int binarySearch(int[] array, int target) {
        int low =0;
        int high = array.length-1;
        int found = -1;
        while ((array.length - 1) > 0) {
            int middlePosition = (array.length - 1) / 2;
            if (array[middlePosition] > target) {
                high = middlePosition;
                array = Arrays.copyOfRange(array, 0, middlePosition);
                System.out.println(toString(array));
            } else if (array[middlePosition] < target) {
                low = middlePosition;

                array = Arrays.copyOfRange(array, middlePosition, array.length-1);
                System.out.println(toString(array));
            } else if (array[middlePosition] == target) {
                System.out.println(toString(array));
                return middlePosition;
            }
        }
        return -1;
    }
}
