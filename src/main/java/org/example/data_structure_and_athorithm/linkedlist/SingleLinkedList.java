package org.example.data_structure_and_athorithm.linkedlist;

import java.util.LinkedList;

public class SingleLinkedList {
    public static void main(String[] args) {
        LinkedList<String>   linkedList = new LinkedList<String>();
        // as Stack
        linkedList.push("Conal");
        linkedList.push("Cham");
        // as Queue
        linkedList.offer("Conal");
        linkedList.offer("Hhe");
        linkedList.add("add");
        System.out.println(linkedList.peek());
        linkedList.pop();
        linkedList.poll();
        linkedList.getLast();
        System.out.println(linkedList.getLast());
        System.out.println(linkedList);
    }
}
