package org.example.data_structure_and_athorithm.stack;

import java.util.Date;

public class History {
    private String content;
    private Date time;

    public History(String content, Date time) {
        this.content = content;
        this.time = time;
    }

    public History(String content) {
        this.content = content;
        this.time = new Date();
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTime() {
        return this.time;
    }

    @Override
    public String toString() {
        return "History{" +
                "content='" + content + '\'' +
                ", time=" + time +
                '}';
    }
}
