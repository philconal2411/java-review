package org.example.data_structure_and_athorithm.stack;

import java.util.Stack;

public class HistoryTracking {
    private Stack<History> undoHistories = new Stack<>();
    private Stack<History> mainHistories = new Stack<>();
    private Stack<History> rootHistories = new Stack<>();
    private Stack<History> redoHistories = new Stack<>();
    private HistoryLog[] historiesLog = new HistoryLog[0];

    private String currentContent = "";

    public void setHistory(String data) {
        History history = new History(data);
        this.mainHistories.push(history);
        this.undoHistories.push(history);
    }

    public void printCurrentData() {
        this.getCurrentContent();
        System.out.println(this.currentContent);
    }

    private void getCurrentContent() {
        this.rootHistories = this.mainHistories;
        this.currentContent = "";
        if (this.rootHistories.empty())
            return;
        this.currentContent += this.rootHistories.pop().getContent();

        this.getCurrentContent();
    }

    public History undo() {
        if (this.undoHistories.empty()) {
            System.out.println("Nothing to undo");
            return null;
        }
        History history = this.mainHistories.pop();
        this.redoHistories.push(history);
        return history;
    }

    public History redo() {
        if (this.redoHistories.empty()) {
            System.out.println("Nothing to redo");
            return null;
        }
        History history = this.redoHistories.pop();
        this.mainHistories.push(history);
        return history;
    }

    public void clearHistory() {
        if (!this.undoHistories.empty())
            this.undoHistories.clear();
        if (!this.redoHistories.empty())
            this.redoHistories.clear();
    }

    public HistoryLog[] log() {
        return historiesLog;
    }
}
