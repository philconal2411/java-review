package org.example.data_structure_and_athorithm.stack;

import java.util.Date;

public class HistoryLog extends History {
    private String message;
    private String type;

    public HistoryLog(String content, Date time, String message, String type) {
        super(content, time);
        this.message = message;
        this.type = type;
    }

    public HistoryLog(String content, String message, String type) {
        super(content);
        this.message = message;
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
