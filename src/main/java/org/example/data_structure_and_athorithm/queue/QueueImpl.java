package org.example.data_structure_and_athorithm.queue;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;

public class QueueImpl {
    public static void main(String[] args) {
//        Queue<String> queue = new LinkedList<>();
//        queue.add("Conal");
//        queue.offer("Kavin");
//        queue.offer("Chad");
//        queue.offer("Harold");
//        queue.poll();
//        queue.poll();
//        queue.poll();
//        System.out.println(queue.peek());
//
        Queue<String> queue = new PriorityQueue<>(Collections.reverseOrder());
        queue.offer("A");
        queue.offer("I");
        queue.offer("C");
        queue.offer("D");
        queue.offer("F");
        while (!queue.isEmpty()){
            System.out.println(queue.poll());
        }
    }
}
