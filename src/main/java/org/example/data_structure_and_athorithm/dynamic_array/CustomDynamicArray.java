package org.example.data_structure_and_athorithm.dynamic_array;

public class CustomDynamicArray {
    int size;
    int capacity = 10;
    Object[] array;

    public CustomDynamicArray() {
        this.array = new Object[capacity];
    }

    public CustomDynamicArray(int capacity) {
        this.capacity = capacity;
        this.array = new Object[capacity];
    }
}
