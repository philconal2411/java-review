package org.example.data_structure_and_athorithm.intepolation_search;

public class Main {
    public static void main(String[] args) {
        int[] array = new int[100];
        for (int i = 0; i < 100; i++) {
            array[i] = i;
        }
        int target = 68;
        int index = interpolationSearch(array, target);

        if (index == -1) {
            System.out.println("Element not found!");
        } else {
            System.out.println("Element found at: " + index);
        }
    }

    public static int interpolationSearch(int[] array, int target) {
        int low = 0;
        int high = array.length - 1;

        while (target >= array[low] && target <= array[high] && low <= high) {
            int probe = low + (high - low) * (target - array[low]) / (array[high] - array[low]);
            System.out.println("probe: " + probe);
            if (array[probe] == target)
                return probe;
            else if (array[probe] < target)
                low = probe + 1;
            else high = probe - 1;
        }
        return -1;
    }
}
