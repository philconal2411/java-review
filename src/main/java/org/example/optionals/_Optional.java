package org.example.optionals;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class _Optional {
    public static void main(String[] args) {
        Consumer<String> consumer = (per) -> System.out.println("Phil Conal");
        Optional.ofNullable("An").ifPresent(consumer);
        Consumer<List<? extends Animal>> optionalAnimal = animalList -> animalList.forEach((animal) -> {
            if (animal != null)
                System.out.println(animal.getName() + " " + animal.getType());
        });
        Optional.of(List.of(new Animal("Alaska", "Dog"), new Animal("PitPub", "Dog"))).ifPresentOrElse(optionalAnimal, () -> {
            throw new NullPointerException("No Animal!!");
        });
    }

    static class Animal {
        private final String name;
        private final String type;

        public Animal(String name, String type) {
            this.name = name;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }
    }
}
