package org.example.bank_management.history;

import org.example.bank_management.account.Account;

import java.util.Date;

public class History {
    private Date trackingDate;
    private String message;
    private double amount;
    private Account toAccount;

    public History(Date trackingDate, String message, double amount, Account toAccount) {
        this.trackingDate = trackingDate;
        this.message = message;
        this.amount = amount;
        this.toAccount = toAccount;
    }

    public History(Date trackingDate, String message) {
        this.trackingDate = trackingDate;
        this.message = message;
    }

    @Override
    public String toString() {
        return message + " at " + this.trackingDate;
    }

    public Date getTrackingDate() {
        return trackingDate;
    }

    public void setTrackingDate(Date trackingDate) {
        this.trackingDate = trackingDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public void setToAccount(Account toAccount) {
        this.toAccount = toAccount;
    }
}
