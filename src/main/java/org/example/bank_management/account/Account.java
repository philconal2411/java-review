package org.example.bank_management.account;

import org.example.bank_management.history.History;
import org.example.bank_management.util.Generate;

import java.util.*;

public class Account extends Customer {
    private String id;
    private String accountNumber;
    private Date dateJoined;
    private AccountType accountType;
    private double balance = 0.0;
    private double withdraw = 0.0;
    private Map<String, History> historyMap = new HashMap<>();

    public Account(String accountNumber, Date dateJoined, AccountType accountType, double balance) {
        this.id = Generate.generateUUID();
        this.accountNumber = accountNumber;
        this.dateJoined = dateJoined;
        this.accountType = accountType;
        this.balance = balance;
        History history = new History(new Date(), "Account " + accountNumber + " created!");
        this.historyMap.put(this.accountNumber, history);
    }

    public Account(String name, String phone, Date dob, String address, String accountNumber, Date dateJoined, AccountType accountType, double balance) {
        super(name, phone, dob, address);
        this.id = Generate.generateUUID();
        this.accountNumber = accountNumber;
        this.dateJoined = dateJoined;
        this.accountType = accountType;
        this.balance = balance;
        History history = new History(new Date(), "Account " + accountNumber + " created!");
        this.historyMap.put(this.accountNumber, history);
    }

    public Account(Customer customer, String accountNumber, Date dateJoined, AccountType accountType, double balance) {
        super(customer.getName(), customer.getAddress(), customer.getDob(), customer.getAddress());
        this.id = Generate.generateUUID();
        this.accountNumber = accountNumber;
        this.dateJoined = dateJoined;
        this.accountType = accountType;
        this.balance = balance;
        History history = new History(new Date(), "Account " + accountNumber + " created!");
        this.historyMap.put(this.accountNumber, history);
    }

    public String getId() {
        return id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Date getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(Date dateJoined) {
        this.dateJoined = dateJoined;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public double getBalance() {
        return this.balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getWithdraw() {
        return withdraw;
    }

    public void setWithdraw(double withdraw) {
        this.withdraw = withdraw;
    }

    public void transferMoney(Account account, double amount) {
        account.balance += amount;
        this.balance -= amount;
        History history = new History(new Date(), "Transfer money to  " + account.getAccountNumber() + amount);
        this.historyMap.put(this.accountNumber + "1", history);
    }

    public void withdraw(double amount) {
        this.withdraw -= amount;
        History history = new History(new Date(), "Checkout money  " + amount);
        this.historyMap.put(this.accountNumber + "2", history);
    }

    public void printLog() {
        List.of(this.historyMap.values()).forEach(System.out::println);
    }
}
