package org.example.bank_management.account;

public enum AccountType {
    PREMIUM("Premium"),
    GOLD("Gold"),
    SILVER("Silver");
    private String accountType;

    AccountType(String type) {
        this.accountType = type;
    }

    public String getAccountType() {
        return this.accountType;
    }
}
