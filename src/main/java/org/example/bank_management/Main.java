package org.example.bank_management;

import org.example.bank_management.account.Account;
import org.example.bank_management.account.AccountType;
import org.example.bank_management.account.Customer;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Customer conal = new Customer("Conal", "0328674312", new Date(), "Lam Dong");
        Customer cham = new Customer("Cham", "0987675432", new Date(), "Lam Dong");
        Customer thanh = new Customer("Conal", "09864556776", new Date(), "Da Lat");


        Account conalAccount = new Account(conal, "234567890-90", new Date(), AccountType.PREMIUM, 100);
        Account chamAccount = new Account(conal, "2345678678490", new Date(), AccountType.GOLD, 100);
        Account thanhAccount = new Account(conal, "6666567890-90", new Date(), AccountType.SILVER, 100);

        conalAccount.transferMoney(chamAccount, 40);
        conalAccount.withdraw(800);
        System.out.println(conalAccount.getBalance());
        System.out.println(chamAccount.getBalance());
        conalAccount.printLog();
    }
}
