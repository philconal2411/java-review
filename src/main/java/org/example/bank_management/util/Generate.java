package org.example.bank_management.util;

import java.util.UUID;

public class Generate {
    public static String generateUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
